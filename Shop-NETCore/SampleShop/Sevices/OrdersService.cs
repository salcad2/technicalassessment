using System;
using System.Collections.Generic;
using System.Linq;
using SampleShop.Interfaces;
using SampleShop.Model;
using SampleShop.Queries;

namespace SampleShop.Services
{
    public class OrdersService : IOrdersService
    {
        private readonly GetAllOrdersQuery queryAll;
        private readonly GetAllItemsQuery queryAllItems;
        private readonly GetOrderByIdQuery queryById;
        private readonly AddOrderQuery queryAdd;
        private readonly DeleteOrderQuery queryDelete;

        private static IDatabase db = DatabaseFactory.CreateDatabase();

        public OrdersService()
        {
            this.queryAll = new GetAllOrdersQuery(db);
            this.queryAllItems = new GetAllItemsQuery(db);
            this.queryById = new GetOrderByIdQuery(db);
            this.queryAdd = new AddOrderQuery(db);
            this.queryDelete = new DeleteOrderQuery(db);
        }

        public OrdersService(GetAllOrdersQuery queryAll, GetAllItemsQuery queryAllItems,
            GetOrderByIdQuery queryById, AddOrderQuery queryAdd, DeleteOrderQuery queryDelete)
        {
            this.queryAll = queryAll;
            this.queryAllItems = queryAllItems;
            this.queryById = queryById;
            this.queryAdd = queryAdd;
            this.queryDelete = queryDelete;
        }


        /// <summary>
        /// Lists all orders that exist in db
        /// </summary>
        public IEnumerable<Order> All()
        {
            return queryAll.Execute().ToList();
        }

        public IEnumerable<Item> AllItem()
        {
            return queryAllItems.Execute().ToList();
        }
        
        /// <summary>
        /// Gets single order by its id
        /// </summary>
        public Order GetById(int id)
        {
            return queryById.Execute(id);
        }

        /// <summary>
        /// Tries to add given order to db, after validating it
        /// </summary>
        public Order Add(Order newOrder)
        {
            if (newOrder == null)
            {
                throw new ArgumentNullException("newOrder");
            }

            var result = ValidateNewOrder(newOrder);
            if ((result & ValidationResult.Ok) == ValidationResult.Ok)
            {
                queryAdd.Execute(newOrder);
                return newOrder;
            }

            return null;
        }

        /// <summary>
        /// Checks whether given order can be added.
        /// Performs logical and business validation.
        /// </summary>
        public ValidationResult ValidateNewOrder(Order newOrder)
        {
            var result = ValidationResult.Default;

            if (newOrder == null)
            {
                throw new ArgumentNullException("newOrder");
            }

            var items = queryAllItems.Execute();

            foreach (var item in newOrder.OrderItems)
            {
                if (item.Value <= 0) result |= ValidationResult.NoItemQuantity; 
                if (!items.Any(p => p.Id == item.Key))
                {
                    result |= ValidationResult.ItemDoesNotExist;
                }
            }

            if (result == ValidationResult.Default)
            {
                result = ValidationResult.Ok;
            }

            return result;
        }

        /// <summary>
        /// Deletes (if exists) order from db (by its id)
        /// </summary>
        public void Delete(int id)
        {
            queryDelete.Execute(id);
        }

        /// <summary>
        /// Returns all orders (listed chronologically) between a given start date and end date.
        /// Start and end dates must be from the past (not in the future or today).
        /// </summary>
        public IEnumerable<Order> GetByDates(DateTime start, DateTime end)
        {
            // TODO
            // Implement method returning all orders (use queryAll) between a give start date and end date.
            // Method should check if dates are in the past (not in the future or today) else
            // throw an ArgumentException. 
            // Return all orders in chronological order. 

            var list = from o in All()
                       where (o.CreateDate >= start.Date && o.CreateDate <= end.Date)
                       select o;

            return list;

            //throw new NotImplementedException();
        }

        /// <summary>
        /// Returns statistics (list of pairs [ItemId, Total] listed by id) on a given day.
        /// Day must be from the past (not in the future or today).
        /// </summary>
        public IEnumerable<ItemSoldStatistics> GetItemsSoldByDay(DateTime day)
        {
            // TODO
            // Implement method returning data described in a method summary.
            // The order have duplicate items, the total have to be summed. 
            // Use queryAll and queryAllOrders.

            var list1 = from i in (All().Where(o => o.CreateDate == day.Date)
                    .SelectMany(o => o.OrderItems))
                group i by i.Key
                into g
                select new ItemSoldStatistics
                {
                    ItemId = g.Key, Total = g.Count()
                };

            var list2 = from i in (All().Where(o => o.CreateDate == day.Date)
                    .SelectMany(o => o.OrderItems))
                select new ItemSoldStatistics
                {
                    ItemId = i.Key,
                    Total = i.Value
                };

            var list3 = from i in list2
                join m in AllItem() on i.ItemId equals m.Id
                select new ItemSoldStatistics
                {
                    ItemId = i.ItemId,
                    Total = i.Total * m.Price
                };

            var groupedProducts = list3.GroupBy(item => item.ItemId);
            var results = groupedProducts.Select(i => new ItemSoldStatistics
            {
                ItemId = i.Key,
                Total = i.Sum(prod => prod.Total)
            });

            return (IEnumerable<ItemSoldStatistics>)results;

            //throw new NotImplementedException();
        }
    }
} 
