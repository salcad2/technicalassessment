using System;
using System.Collections.Generic;
using System.Linq;
using SampleShop.Interfaces;
using SampleShop.Model;
using SampleShop.Queries;

namespace SampleShop.Services
{
    public class ItemsService : IItemsService
    {
        private readonly GetAllItemsQuery _query;
        private IDatabase db;

        public ItemsService()
        {
            db = DatabaseFactory.CreateDatabase();
            _query = new GetAllItemsQuery(db) ;
        }

        /// <summary>
        /// Lists all items that exist in db
        /// </summary>
        public IEnumerable<Item> All()
        {
            return _query.Execute().ToList();
        }
    }
}
