import React from 'react';
import { prototype } from 'stream';

export const Message = props => <div className="text-center">
    {props.header==undefined? null: <h3 className="message-header">{props.header}</h3>}
    {props.text==undefined? <div class="message-body">{props.children}</div>:<div class="message-body">{props.text}</div>}
</div>
