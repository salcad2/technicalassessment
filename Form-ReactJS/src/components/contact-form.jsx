import React from 'react';
import { object,func } from 'prop-types';

export class ContactForm extends React.Component{

    static defaultProps = {
        data:{
            name:'',
            email:'',
            option:'',
            select: '',
            message:'',
            terms:false
        }
      
    }

    static propTypes = {
        onChange: func.isRequired,
        onSubmit: func.isRequired,
        data: object.isRequired
    }

    constructor(props){
        super(props)
        
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fieldChange = this.fieldChange.bind(this);
    }

    /**
     * When form is submitted forward contact data to parent
     * @param {event} DOMEvent
     */
    handleSubmit(event){
        event.preventDefault();

        this.props.onSubmit(this.props.data)
    }

    fieldChange(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;

        if (name == 'name') {
            this.props.data.name = value;
        } else if (name == 'email') {
            this.props.data.email = value;
        } else if (name == 'option') {
            this.props.data.option = value;
        } else if (name == 'select') {
            this.props.data.select = value;
        } else if (name == 'message') {
            this.props.data.message = value;
        } else if (name == 'terms') {
            this.props.data.terms = value;
            if (value) {
                target.value = "on"
            } else {
                target.value = "off"
            } 
        }

        this.props.onChange(this.props.data);
    }

    isSelected(key, option){
        return this.props.data[key] == option
    }

    options = [
        {id:1, label:'I have question about my membership'},
        {id:2, label:'I have technical question'},
        {id:3, label:'I would like to change membership'},
        {id:4, label:'Other question'},
    ]

    render(){
        let data = this.props.data;

        return <form onSubmit={this.handleSubmit}>

        <h3>Contact Form</h3>

        <div class="form-group">
            <label className="form-label">Your Name:</label>
            <input name="name" className="form-control" onChange={this.fieldChange} value={this.props.data.name}/>
        </div>

        <div class="form-group">
            <label className="form-label">Your Best Email:</label>
            <input name="email" className="form-control" onChange={this.fieldChange} value={this.props.data.email}/>
        </div>

        <label className="form-label">Select your membership option:</label>
        <div class="form-group row">
            <label className="form-label col-xs-4">
            <input type="radio" name="option" value="A" onChange={this.fieldChange} checked={this.props.data.option === 'A'} /> Option A</label>
            <label className="form-label col-xs-4">
            <input type="radio" name="option" value="B" onChange={this.fieldChange} checked={this.props.data.option === 'B'} /> Option B</label>
            <label className="form-label col-xs-4">
            <input type="radio" name="option" value="C" onChange={this.fieldChange} checked={this.props.data.option === 'C'} /> Option C</label>
        </div>

        <hr/>

        <div class="form-group">
            <label className="form-label">What can we help you with:</label>
            <select  name="select" className="form-control"  onChange={this.fieldChange} value={this.props.data.select}>
                <option value="1" label="1">I have question about my membership 1</option>
                <option value="2" label="2">I have question about my membership 2</option>
                <option value="3" label="3">I have question about my membership 3</option>
            </select>
        </div>

        <div class="form-group">
            <label className="form-label">Message:</label>
            <textarea name="message" rows="10" onChange={this.fieldChange} value={this.props.data.message} placeholder="Please type your question here"  className="form-control"/>
        </div>

        <div class="form-group">
            <label className="form-label"> <input name="terms" type="checkbox" onChange={this.fieldChange} checked={this.props.data.terms}/> I agree to terms and conditions </label>
        </div>
          <input type="submit" value="Send" className="contactform-submit" />
        </form>
    }
}
